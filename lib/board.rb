class Board
  attr_accessor :grid, :default_grid

  def initialize(grid = nil)
    grid ||= Board.default_grid
    @grid = grid
  end

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    grid[row][col] = mark
  end

  def count
    grid.flatten.count(:s)
  end

  def empty?(pos = nil)
    if pos == nil
      count == 0 #if pos.nil?
    else
      @grid[pos[0]][pos[1]] != :s
    end
  end

  def full?
    grid.flatten.count(nil) == 0
  end

  def won?
    count == 0
  end

  def random_position
    pos = [rand(grid.length), rand(grid.first.length)]
  end

  def place_random_ship(ship = :s)

    raise "The Board is full" if full?

    # pos = random_position

    # if empty?
    #   # pos = [rand(0..grid.length), rand(0..grid.first.length)]
    #   self[pos] = :s
    # end

    # until empty?(pos)
      # pos = random_position
    position = open_positions(grid).sample
      # self[pos] = :s
      # @grid[open_positions(grid).sample] = :s
    # end

    self[position] = ship
  end

  # this is where you integrate the ship class
  def place_ship(position = open_positions(grid).sample, ship = :s)
    self[position] = ship
  end

  def open_positions(grid)
    result = []
    grid.each_with_index do |e, i|
      e.each_with_index do |f, j|
        if f.nil?
          result << [i, j]
        end
      end
    end
    result
  end

  # does not show ships
  def display
    # puts "Here is the current board"
    puts grid.map {|line| line.map { |spot| (spot == :x) ? spot : spot = "_"}.join("") }.join("\n")
  end

  # shows ships - for showing final board and for testing purposes
  def to_s
    puts grid.map {|line| (line.map { |spot| spot ? spot : spot = "_"}.join("")) }.join("\n")
  end

end
