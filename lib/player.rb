class HumanPlayer
  # attr_accessor :get_play , :numeric, :parse

  # def parse(string)
  #   result = []
  #   string.chars.each do |e|
  #     result << e.to_i if numeric?(e)
  #   end
  #   result
  # end
  #
  # def numeric(letter)
  #   Float(letter) != nil rescue false
  # end

  def get_play
    puts "Where would you like to fire?"

    input = $stdin.gets.chomp
    # array = []
    # array = input.split(", ").map { |e| e.to_i }
    parse_play(input)

  end

  # method only works for boards with indexes 0 to 9, reduces input errors
  def parse_play(reply)
    result = arrayify(reply)
    result = [rand(9), rand(9)] if result.length < 2  # fire random if bad input
    result
  end

  def arrayify(reply)
    result = []
    reply.chars.each {|entry| result << (entry.to_i) if "0123456789".include?(entry) }
    result
  end

  def place_ship
    puts "Where would you like to place a ship?"
    input = $stdin.gets.chomp
    # array = []
    # array = input.split(", ").map { |e| e.to_i }
    # array
    # needs to go interact with board and ship class to put a ship on, the same way fire works
    arrayify(input)
  end

end
