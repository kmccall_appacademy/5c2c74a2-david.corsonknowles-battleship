class Ship

  SHIPS = {
    carrier: Array.new(5, :c),
    battleship: Array.new(4, :b),
    submarine: Array.new(3, :s),
    destroyer: Array.new(3, :d),
    patrol_boat: Array.new(3, :b)
  }

  #not yet used
  def sunk!(ship)
    how_long = ship.length
    0.upto(how_long) do |i|
      ship[i] = :x
    end
    ship
  end

  def sunk?(ship)
    ship.all? {|e| e == :x}
  end

end
