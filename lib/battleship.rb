require_relative "board"
require_relative "player"
require_relative "computer_player"
require_relative "ship"

class BattleshipGame
  attr_reader :board, :player, :move, :player2, :board2

  def initialize(player = nil, board = nil, player2 = nil, board2 = nil)
    @player = player
    @board = board
    @player2 = player2
    @board2 = board2
  end

  # not yet used
  def setup
    board2.place_ship(player.place_ship, :s)
    board.place_ship(player2.place_ship, :s)
  end

  def attack(pos, specific_board = nil)
    board[pos] = :x if specific_board == nil
    specific_board[pos] = :x if specific_board
  end

  def count
    board.count
  end

  def game_over?
    board.won? || (board2.won? if board2)
  end

  def play_turn
    get_play = player.get_play
    attack(get_play)
    p "player1 fires at #{get_play}"

    if player2
      get_play = player2.get_play
      attack(get_play, board2)
      p "player2 fires at #{get_play}"
    end
  end

  def play
    # board.place_random_ship
    # board2.place_random_ship if board2
    player2 ? setup : board.place_random_ship


    until game_over?
      puts "#{player}"
      board.display
      puts "#{player2}" if player2
      board2.display if board2
      play_turn
    end
    conclusion
  end

  def conclusion
    puts "Kaplow! Here are the final boards:"
    puts "#{player} won? #{board.won?}"
    # puts board.won?
    puts board.to_s
    puts "#{player2} won? #{board2.won?}" if player2 && board2
    # puts  board2.won? if board2
    puts board2.to_s if board2
  end

end

if __FILE__ == $PROGRAM_NAME
  player = ComputerPlayer.new
  board = Board.new
  player2 = HumanPlayer.new
  board2 = Board.new
  game = BattleshipGame.new(player, board, player2, board2)
  game.play

end
